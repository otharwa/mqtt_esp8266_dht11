Link Source Schematick: https://www.hackster.io/Manoranjan2050/dht11-and-nodemcu-with-blynk-10e6b1
Base para generar metricas: http://nilhcem.com/iot/home-monitoring-with-mqtt-influxdb-grafana
Y su github: https://github.com/Nilhcem/home-monitoring-grafana


Requerimientos para compilar:
 - ArduinoJson
 - PubSubClient
 - U8g2
 - "DHT sensor library" by Adafruit
 - "Adafruit Unified Sensor" by Adafruit 



IDEAS:
 WebServer, ajax, interfas bonita para mostrar temperatura y humedad desde el ESP8266 (Buena documentacion y schematics ): https://lastminuteengineers.com/esp8266-dht11-dht22-web-server-tutorial/

 Data Logger Interno con grafica: https://circuits4you.com/2019/01/25/esp8266-dht11-humidity-temperature-data-logging/

 Aprender structuras de codigo: https://www.hackster.io/14872/temperature-logging-using-mqtt-and-mongodb-a58cce

 Aplicacion piola para Android: https://play.google.com/store/apps/details?id=net.routix.mqttdash

 Demaciadas ideas todas juntas: https://air.imag.fr/index.php/Developing_IoT_Mashups_with_Docker,_MQTT,_Node-RED,_InfluxDB,_Grafana#Using_MQTT_for_collecting_sensors_data

 Node.js MQTT https://github.com/mcollina/mosca

RECURSOS: 

 JsonPath: https://github.com/json-path/JsonPath/blob/master/README.md
 Generar graficos bonitos: https://grafana.com/