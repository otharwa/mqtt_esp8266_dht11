/**
 * Renombrar este archivo a config_network.h y colocar los valores correspondientes a tu red.
 */
const char *ssid			= "NOMBRE DE LA RED WIFI";
const char *password		= "CLAVE";
const char *mqtt_server		= "SERVIDOR_MQQT.local";
const char *mqtt_pub		= "test1";				// Canal de publicacion simple
const char *mqtt_pub_json	= "cultivo_json"; // Canal de publicacion en formato JSON
const char *mqtt_sub		= "cultivo";			// Suscripcion de mensajes