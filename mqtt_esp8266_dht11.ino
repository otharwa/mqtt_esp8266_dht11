// Modelo de pantalla 128x32 pixeles SSD1306
#include "config.h"
#include "config_network.h"
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <U8g2lib.h>

// Dependencias pantalla
#ifdef U8X8_HAVE_HW_SPI
#include <SPI.h>
#endif
#ifdef U8X8_HAVE_HW_I2C
#include <Wire.h>
#endif
U8G2_SSD1306_128X32_UNIVISION_F_SW_I2C u8g2(U8G2_R0, /* clock=*/ SCL, /* data=*/ SDA, /* reset=*/ U8X8_PIN_NONE);   // Adafruit Feather ESP8266/32u4 Boards + FeatherWing OLED
/// END - Dependencias pantalla

WiFiClient espClient;
PubSubClient client(espClient);

long lastMsg = 0;
char msg[50];
char msgTest2[50];
char msgJSON[50];



void espera(int x){
  int periodo = 1000;
  unsigned long TiempoAhora = 0;
  TiempoAhora = millis();
  while(millis() < TiempoAhora+x){
    // espere [periodo] milisegundos
  }  
}

void setup() {
  u8g2.begin();
  // pinMode(BUILTIN_LED, OUTPUT);     // Initialize the BUILTIN_LED pin as an output
  Serial.begin(115200);
  dht_setup();
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
}

void loop() {
  if (!client.connected()) {
    reconnect();
  }
  client.loop();

  long now = millis();
  if (now - lastMsg > 5000) {
    lastMsg = now;


    dht_med dht_measure = dht_sensor();
    float tem_med = dht_measure.tem;
    float hum_med = dht_measure.hum;

    Serial.print("ROW TEMP: ");
    Serial.println(tem_med);
    Serial.print("ROW HUM: ");
    Serial.println(hum_med);
    // Para que lo tome la aplicacion de arduino
    sprintf(msg, "%.02f", tem_med); 
    sprintf(msgTest2, "%.02f", hum_med); 

    // Para que trabajar via json api
    sprintf(msgJSON, "{\"temp\": %.02f, \"hum\": %.02f, \"id\":\"locotos\"}", tem_med, hum_med); //convertir a char*

    Serial.print("Publish message: ");
    Serial.println(msg);
    client.publish(mqtt_pub, msg);
    client.publish(mqtt_pub_json, msgJSON);
    client.publish("test2", msgTest2);

    u8g2.clearBuffer();          // clear the internal memory
    char textoPantalla[50];
    sprintf(textoPantalla, "T:%.02f - H:%.02f", tem_med, hum_med); 
    u8g2.setFont(u8g2_font_ncenB08_tr); // choose a suitable font
    u8g2.drawStr(0,8,textoPantalla); // write something to the internal memory
    u8g2.sendBuffer();          // transfer internal memory to the display
  }
    // espera(5000);
}
