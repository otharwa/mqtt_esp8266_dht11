#include <DHT.h>

#define DHTPIN D4  //DHT 
#define DHTTYPE DHT11  //DHT

DHT dht(DHTPIN, DHTTYPE); 

void dht_setup(){
  pinMode(DHTPIN, INPUT);
}

dht_med dht_sensor(){
  dht_med measure; 
  measure.hum = dht.readHumidity();
  measure.tem = dht.readTemperature();
  if (isnan(measure.hum) || isnan(measure.tem)){
    Serial.println();
    Serial.print("Error DHT:");
    Serial.println();    
  }
  return measure;
}
